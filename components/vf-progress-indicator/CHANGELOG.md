### 1.0.1

* Fix: Corrected display of progress indicator - [Tracking issue](https://github.com/visual-framework/vf-core/issues/2135).

### 1.0.0

* Updated status to live - [Tracking issue](https://github.com/visual-framework/vf-core/issues/2170).

### 1.0.0-alpha.3

* Basic implementation of vf-progress-indicator
* [Tracking discussion](https://github.com/visual-framework/vf-core/discussions/1648)
