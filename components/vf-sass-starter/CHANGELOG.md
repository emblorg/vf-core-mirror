### 0.1.31

* Updated status to live - [Tracking issue](https://github.com/visual-framework/vf-core/issues/2170).

### 0.1.30

* Dependency bump.

### 0.1.29

* Dependency bump.

### 0.1.28

* Dependency bump.

### 0.1.27

* Dependency bump.

### 0.1.26

* Dependency bump.

### 0.1.25

* Dependency bump.

### 0.1.24

* Dependency bump.

### 0.1.23

* Dependency bump.

### 0.1.19

* dependency bump

### 0.1.13

* dependency bump

### 0.1.12

* dependency bump

### 0.1.7

* dependency bump

### 0.1.4

* dependency bump

### 0.1.2

* dependency bump

### 0.1.1

* dependency bump

### 0.1.0

* updates package.json to dependencies rather than devDependencies
* updates relative paths of index.scss to target installed packages in node_modules

### 0.0.3

* Initial version
