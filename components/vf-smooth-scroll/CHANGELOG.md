### 1.0.0

* Updated status to live - [Tracking issue](https://github.com/visual-framework/vf-core/issues/2170).

### 1.0.0-alpha.2

* Updated documentation

### 1.0.0-alpha.1

* Introduce CSS-based smooth scroll component.
