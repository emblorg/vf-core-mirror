### 1.0.3

* Changed: Test cases added for Angular  [Tracking issue](https://github.com/visual-framework/vf-core/issues/1932)
* Updated status to live - [Tracking issue](https://github.com/visual-framework/vf-core/issues/2170).
* Dependency update to be compatible with ESM packages[Tracking issue](https://github.com/visual-framework/vf-core/issues/2107)

### 1.0.2

* Added : Experimental Angular support for Tabs [Tracking issue](https://github.com/visual-framework/vf-core/issues/2088)

### 1.0.1

* Updated the Back to Top button to secondary and changed arrow colour.
  * [Tracking issue](https://github.com/visual-framework/vf-core/issues/1956)

### 1.0.0-alpha.2

* Fixed a null reference error.
  * [Tracking issue](https://github.com/visual-framework/vf-core/issues/1567)

### 1.0.0-alpha.1

* Basic implementation of vf-back-to-top component
* [Tracking issue](https://github.com/visual-framework/vf-core/issues/1483)
