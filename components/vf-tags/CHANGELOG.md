### 1.0.0

* Updated status to live - [Tracking issue](https://github.com/visual-framework/vf-core/issues/2170).

### 1.0.0-alpha.1

* Creates a prototype MVP vf-tags component that can be easily improved in the future.
* https://github.com/visual-framework/vf-core/issues/875
